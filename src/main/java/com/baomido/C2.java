package com.baomido;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.util.docx.DocxUtil;
import com.util.excel.FileUtilH;
import com.util.excel.Listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/5/21
 **/
public class C2 extends C1 {
    public static void main(String[] args) throws Exception {
        Queue<String> work = getWork();
        DocxUtil docxUtil = new DocxUtil();
        docxUtil.addH("物联网现网能力测试及关键技术研发项目双周报", 1);
        for (String s : getUser()) {
            docxUtil.addH(s, 2);
            docxUtil.addP("本双周工作内容:");
            for (int i = 1; i <= 3; i++) {
                if (work.size() == 0) break;
                docxUtil.addP(i + "." + work.remove());
            }
        }
        docxUtil.createWord("D:\\文档\\物联网双周报\\2022\\大唐-物联网现网能力测试及关键技术研发项目双周报2022.9.19-2022.10.14.docx");
    }

    public static String[] getUser() {
        String s = "孙军杰、巴合提、曹溢滨、武春宏、赵盛平、邹清、段朋、孟祥磊、廖云辉、徐文龙、张耀辉、邹韫、饶燕、李亚川、贡宏才、余超、路海亮、李翔、杨入菁、任超翔、李旺";
        return s.split("、");
    }

    public static Queue<String> getWork() {
        Queue<String> list = new LinkedList<>();
        try {
            BufferedReader reader = FileUtil.getReader("/temp/t.txt", "utf-8");
            String line = reader.readLine();
            while (line != null) {
                list.add(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
