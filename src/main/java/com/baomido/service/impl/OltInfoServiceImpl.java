package com.baomido.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomido.dao.OltInfoDao;
import com.baomido.entity.OltInfo;
import org.springframework.stereotype.Service;

/**
 * (OltInfo)表服务实现类
 *
 * @author makejava
 * @since 2024-11-19 16:45:41
 */
@Service("oltInfoService")
public class OltInfoServiceImpl extends ServiceImpl<OltInfoDao, OltInfo> {

}

