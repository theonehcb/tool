package com.baomidomybatisplus.samples.generator.system;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.*;
import org.docx4j.dml.wordprocessingDrawing.Inline;

import javax.xml.bind.JAXBElement;
import java.io.File;
import java.util.List;

public class PPPOE {

    public static void main(String[] args) throws Exception {
        // Load the source document containing the table
        WordprocessingMLPackage srcDoc = WordprocessingMLPackage.load(new File("D:\\temp\\2404\\test.docx"));

        // Get the main document part of the source document
        MainDocumentPart srcMainPart = srcDoc.getMainDocumentPart();

        // Find the first table in the source document
        List<Object> srcContent = srcMainPart.getContent();
        Tbl srcTable = null;
        for (Object content : srcContent) {
            if (content instanceof JAXBElement) {
                srcTable = (Tbl) ((JAXBElement) content).getValue();
                break;
            }
        }

        if (srcTable == null) {
            System.out.println("No table found in the source document.");
            return;
        }

        // Create a new Word document package for the destination document
        WordprocessingMLPackage destDoc = WordprocessingMLPackage.createPackage();

        // Get the main document part of the destination document
        MainDocumentPart destMainPart = destDoc.getMainDocumentPart();

        // Clone the source table and add it to the destination document

        destMainPart.getContent().add(srcTable);

        // Save the destination document
        destDoc.save(new File("/temp/2404/dest.docx"));
    }
}
