//package com.baomidomybatisplus.samples.generator.system.controller;
//
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.stereotype.Controller;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.util.HashMap;
//
///**
// * <p>
// *  前端控制器
// * </p>
// *
// * @author baomidou
// * @since 2024-02-22
// */
//@Controller
//@RequestMapping("/system/pppoe")
//public class PppoeController {
//    private static final String PROVINCE_HEADER = "省份";
//    private static final String CITY_HEADER = "城市";
//    private static final String COVER_NO_HEADER = "覆盖编号";
//
//    public void processParams(
//            String fiveGParamType,
//            HashMap<String, String> fiveGProvinceMap,
//            HashMap<String, String> fiveGCityMap,
//            String fiveGInputFilePath,
//            String fiveGOutputFilePath,
//            HashMap<String, String> baseStationMap
//    ) throws IOException {
//        File fiveGInputFile = new File(fiveGInputFilePath);
//        if (!fiveGInputFile.exists()) {
//            System.err.println("4G工参文件不存在，退出程序");
//            System.exit(1);
//        }
//
//        try (BufferedReader reader = new BufferedReader(new FileReader(fiveGInputFile));
//             BufferedWriter writer = new BufferedWriter(new FileWriter(fiveGOutputFilePath))) {
//
//            // 构造表头
//            StringBuilder headerBuilder = new StringBuilder();
//            for (String header : reader.readLine().split(",")) {  // 假设表头以逗号分隔
//                headerBuilder.append(header).append(",");
//            }
//            headerBuilder.append(PROVINCE_HEADER).append(",")
//                    .append(CITY_HEADER).append(",")
//                    .append(COVER_NO_HEADER);
//
//            // 写入表头到结果文件
//            writer.write(headerBuilder.toString().trim());
//
//            // 进行后续处理...
//        }
//    }
//}
//
//public class National5GWorkParameterProcessor {
//
//    private final GisService gisService;
//    private final MacroStationService macroStationService;
//
//    public National5GWorkParameterProcessor(GisService gisService, MacroStationService macroStationService) {
//        this.gisService = gisService;
//        this.macroStationService = macroStationService;
//    }
//
//    // 读取全国5G工参数据文件
//    public List<WorkParameter> readNationalWorkParameters(String filePath) throws IOException {
//        List<String> lines = Files.readAllLines(Path.of(filePath));
//        return parseWorkParameters(lines);
//    }
//
//    // 将全国5G工参数据写入GIS库
//    public WriteResult writeNationalWorkParametersToGis(List<WorkParameter> workParameters) {
//        return gisService.uploadWorkParameters(workParameters);
//    }
//
//    // 按省份拆分数据操作
//    public Map<String, List<WorkParameter>> splitWorkParametersByProvince(List<WorkParameter> workParameters) {
//        Map<String, List<WorkParameter>> provinceDataMap = new HashMap<>();
//        for (WorkParameter wp : workParameters) {
//            String province = wp.getProvince();
//            if (!provinceDataMap.containsKey(province)) {
//                provinceDataMap.put(province, new ArrayList<>());
//            }
//            provinceDataMap.get(province).add(wp);
//        }
//        return provinceDataMap;
//    }
//
//    // 读取GIS库分省拆分数据
//    public Map<String, List<WorkParameter>> readProvincialWorkParametersFromGis() {
//        // 实际操作中，您可能需要调用GisService的相关方法来实现
//        // 这里仅作为示例，假设已经从GIS库获取到数据并进行了省份分类
//        Map<String, List<WorkParameter>> provincialDataMap = gisService.downloadProvincialWorkParameters();
//        return provincialDataMap;
//    }
//
//    // 按照省份写入31个省份文件数据
//    public Map<String, WriteResult> writeProvincialDataToFile(Map<String, List<WorkParameter>> provinceDataMap) {
//        Map<String, WriteResult> writeResults = new HashMap<>();
//        for (Map.Entry<String, List<WorkParameter>> entry : provinceDataMap.entrySet()) {
//            String province = entry.getKey();
//            List<WorkParameter> provincialData = entry.getValue();
//            WriteResult result = writeWorkParametersToFile(province, provincialData);
//            writeResults.put(province, result);
//        }
//        return writeResults;
//    }
//
//    // 提取5G工参宏站数据
//    public List<MacroStationWorkParameter> extractMacroStationData(List<WorkParameter> workParameters) {
//        // 实现逻辑：筛选出属于宏站的数据
//        List<MacroStationWorkParameter> macroStationData = new ArrayList<>();
//        for (WorkParameter wp : workParameters) {
//            if (wp.isMacroStation()) {
//                macroStationData.add((MacroStationWorkParameter) wp);
//            }
//        }
//        return macroStationData;
//    }
//
//    // 查询GIS库读取各省宏站工参结果
//    public Map<String, List<MacroStationWorkParameter>> queryMacroStationsFromGis() {
//        // 实际操作中，您可能需要调用GisService的相关方法来实现
//        // 这里仅作为示例，假设已经从GIS库获取到各省宏站工参结果
//        Map<String, List<MacroStationWorkParameter>> macroStationsByProvince = gisService.queryMacroStations();
//        return macroStationsByProvince;
//    }
//}
//
//public class SiteService {
//
//    private final SiteRepository siteRepository;
//
//    public SiteService(SiteRepository siteRepository) {
//        this.siteRepository = siteRepository;
//    }
//
//    /**
//     * 读取5G工参数据，根据经纬度信息查询50m对应站点ID
//     */
//    public List<String> findSiteIdsWithin50m(double latitude, double longitude) {
//        return siteRepository.getAllSites().stream()
//                .filter(site -> isWithinDistance(site.getLatitude(), site.getLongitude(), latitude, longitude, 50))
//                .map(Site::getId)
//                .collect(Collectors.toList());
//    }
//
//    /**
//     * 判断两个站点是否在指定距离内（单位：米）
//     */
//    private boolean isWithinDistance(double lat1, double lon1, double lat2, double lon2, double distance) {
//        // Haversine formula implementation to calculate the distance between two points
//        // ... (omitted for brevity, see https://en.wikipedia.org/wiki/Haversine_formula)
//
//        return calculatedDistance <= distance;
//    }
//
//    /**
//     * 更新5G工参中某个站点的id50m字段信息
//     */
//    public void updateId50mForSite(String siteId, String id50mValue) {
//        Site site = siteRepository.findById(siteId).orElseThrow(() -> new IllegalArgumentException("Site not found"));
//        site.setId50m(id50mValue);
//        siteRepository.save(site);
//    }
//
//    /**
//     * 点击查询站点统计（此方法的具体实现取决于您需要统计什么信息，这里仅作为占位符）
//     */
//    public void clickToQuerySiteStatistics() {
//        GISRepository gisRepository = new PostGISRepository();
//
//        // 读取全国室分工参数据CSV文件
//        List<SiteDeploymentParam> deploymentParams = readSiteDeploymentParamsFromCSV("deployment_params.csv");
//        System.out.println("室分工参数据读取完成");
//
//        // 将室分工参数据写入GIS库
//        int deploymentResultCount = gisRepository.writeSiteDeploymentParams(deploymentParams);
//        System.out.printf("室分工参写入GIS库完成，共写入 %d 条数据\n", deploymentResultCount);
//
//        // 返回室分工参写入结果
//        System.out.println("室分工参写入结果: " + (deploymentResultCount > 0 ? "成功" : "失败"));
//
//        // 读取全国室分规划工参数据CSV文件
//        List<SitePlanningParam> planningParams = readSitePlanningParamsFromCSV("planning_params.csv");
//        System.out.println("室分规划工参数据读取完成");
//
//        // 将室分规划工参数据写入GIS库
//        int planningResultCount = gisRepository.writeSitePlanningParams(planningParams);
//    }
//    // 写入5G 700M工参GIS库表
//    public void writeSevenHundredM5GParametersToGIS(List<SevenHundredM5GParameter> parameters) {
//        // 实现将参数数据写入GIS库表的逻辑
//        gisService.write(parameters);
//    }
//
//    // 返回700M工参写入结果
//    public WriteResult getSevenHundredMWriteResult() {
//        // 获取并返回最近一次写入700M工参到GIS库的结果
//        return writeResultService.getLastSevenHundredMWriteResult();
//    }
//
//    // 执行规划数据入库方法
//    public void executePlanningDataImport() {
//        // 调用规划数据入库的具体实现方法
//        planningDataService.importData();
//    }
//
//    // 读取全国规划工参数据文件
//    public PlanningData readNationalPlanningDataFile(String filePath) {
//        // 实现从指定路径读取全国规划工参数据文件的逻辑
//        File file = new File(filePath);
//        // 假设这里使用某种方式（如CSV解析器）读取文件并转换为PlanningData对象
//        return fileReader.read(file);
//    }
//    // 获取图层服务器地址
//    LayerServer layerServer = new LayerServer();
//    String layerServerAddress = layerServer.getAddress();
//        System.out.println("图层服务器地址: " + layerServerAddress);
//
//    // 获取待加载栅格基站信息
//    RasterBaseStationService baseStationService = new RasterBaseStationService();
//    List<BaseStation> baseStations = baseStationService.getRasterBaseStations();
//
//    // 遍历基站并获取其覆盖区域
//        for (BaseStation baseStation : baseStations) {
//        CoverageArea coverageArea = baseStation.getCoverageArea();
//
//        // 遍历栅格
//        for (Raster raster : coverageArea.getRasters()) {
//            // 获取MR栅格RSRP数据小区
//            List<CellTower> cellTowers = raster.getMRRasterRSRPData();
//
//            // 打印小区ID和RSRP值
//            for (CellTower cellTower : cellTowers) {
//                System.out.println("小区ID: " + cellTower.id + ", RSRP: " + cellTower.rsrp);
//            }
//        }
//    }
//}
//}
//
//
//
//}
//
//public class DataProcessor {
//    private List<DataPoint> totalData;
//
//    public DataProcessor(List<DataPoint> totalData) {
//        this.totalData = totalData;
//    }
//
//    /**
//     * 生成两个DataPoint之间的距离值
//     */
//    public double generateDistance(DataPoint point1, DataPoint point2) {
//        return point1.calculateDistance(point2);
//    }
//
//    /**
//     * 改变数据/总数据
//     */
//    public void changeTotalData(List<DataPoint> newData) {
//        this.totalData = newData;
//    }
//
//    /**
//     * 进行距离排序（假设是对所有数据点与指定目标点的距离进行排序）
//     */
//    public List<DataPoint> sortDataByDistance(DataPoint target, List<DataPoint> data) {
//        data.sort(Comparator.comparingDouble(target::calculateDistance));
//        return data;
//    }
//
//    /**
//     * 生成前五数据推荐（按距离排序后的结果）
//     */
//    public List<DataPoint> generateTopFiveRecommendations(DataPoint target) {
//
//
//
//        BuildingSelection selection = new BuildingSelection();
//
//        // 初始化建筑物和室内小区列表
//
//        double rightBoundary = selection.calculateRightBoundary();
//        double topBoundary = selection.calculateTopBoundary();
//        double bottomBoundary = selection.calculateBottomBoundary();
//
//        selection.performInitialBuildingFiltering();
//        List<Building> buildingsInRegion = selection.generateBuildingsInRegion();
//
//        selection.performInitialIndoorCommunityFiltering();
//        List<IndoorCommunity> indoorCommunitiesInRegion = selection.generateIndoorCommunitiesInRegion();
//
//        selection.performSecondaryBuildingFiltering();
//        selection.performSecondaryIndoorCommunityFiltering();
//
//        List<DataPoint> sortedData = sortDataByDistance(target, new ArrayList<>(totalData));
//        return sortedData.subList(0, Math.min(5, sortedData.size()));
//    }
//    public List<Building> generateBuildingsInRegion(String regionId) {
//        // 1. 生成区域内切换对数据
//        List<BuildingData> buildingDataList = dataSwitcher.switchDataForRegion(regionId);
//
//        // 2. 进行建筑物初选过滤
//        List<Building> preliminaryFilteredBuildings = new ArrayList<>();
//        for (BuildingData buildingData : buildingDataList) {
//            if (initialFilter.shouldInclude(buildingData)) {
//                preliminaryFilteredBuildings.add(new Building(buildingData));
//            }
//        }
//
//        // 3. 生成区域内建筑物
//        return preliminaryFilteredBuildings;
//    }
//}