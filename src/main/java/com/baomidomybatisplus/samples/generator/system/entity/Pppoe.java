package com.baomidomybatisplus.samples.generator.system.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2024-02-22
 */
public class Pppoe implements Serializable {

    private static final long serialVersionUID = 1L;

    private String account;

    private String ipPon;

    private LocalDateTime onlineTime;

    private String sn;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getIpPon() {
        return ipPon;
    }

    public void setIpPon(String ipPon) {
        this.ipPon = ipPon;
    }

    public LocalDateTime getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(LocalDateTime onlineTime) {
        this.onlineTime = onlineTime;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    @Override
    public String toString() {
        return "Pppoe{" +
        "account = " + account +
        ", ipPon = " + ipPon +
        ", onlineTime = " + onlineTime +
        ", sn = " + sn +
        "}";
    }
}
