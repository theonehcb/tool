package com.baomidomybatisplus.samples.generator.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

public interface HeMapper<T> extends BaseMapper<T> {
    @Update("truncate table 小区维度")
    void clear();
}
