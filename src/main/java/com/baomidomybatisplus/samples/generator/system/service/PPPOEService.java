package com.baomidomybatisplus.samples.generator.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidomybatisplus.samples.generator.system.mapper.PPPOEMapper;
import com.baomidomybatisplus.samples.generator.system.PPPOE;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class PPPOEService extends ServiceImpl<PPPOEMapper, PPPOE> {
    public void clear(){
        baseMapper.clear();
    }
}