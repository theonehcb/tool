package com.leetcode;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.io.FileUtil;
import com.Util;
import com.test.Alarm;
import com.util.excel.FileUtilH;
import com.util.file.ZipUtil;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;


/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/12
 **/
public class Code {
    private static final Logger log = LoggerFactory.getLogger(Code.class);

    class UnionFind {

        private final int[] fa;

        public UnionFind(int size) {
            fa = new int[size];
            for (int i = 1; i < size; i++) {
                fa[i] = i;
            }
        }

        public int find(int x) {
            if (fa[x] != x) {
                fa[x] = find(fa[x]);
            }
            return fa[x];
        }

        public void merge(int x, int y) {
            fa[find(x)] = find(y);
        }
    }

    int mod = (int) (1e9 + 7);


    public int pow(int a, int b) {
        int res = 1;
        while (b > 0) {
            if (b % 2 == 1) {
                res *= a;
            }
            a *= a;
            b /= 2;
        }
        return res;
    }


    int[][] pos = new int[][]{
            {0, 1}, {0, -1}, {1, 0}, {-1, 0}
    };


    //第一个小于当前值的下标
    public static int firstMin(int[] ints, int t) {
        int l = 0, r = ints.length - 1;
        while (l < r) {
            int mid = (l + r) >> 1;
            if (ints[mid] < t) {
                l = mid + 1;
            } else {
                r = mid;
            }
        }
        if (ints[l] < t) return l;
        else {
            return l - 1;
        }
    }


    //第一个小于当前值的下标
    public int numberOfAlternatingGroups(int[] colors, int k) {
        int l=0,res=0;
        int length = colors.length;
        int cnt=0;
        while (l<length){
            if(colors[l]!=colors[(l-1+length)%length]){
                cnt++;
            }else {
               cnt=0;
            }
            if(cnt>=k)res++;
            l++;
        }
        return res;
    }

    public List<Integer> asList(int ...v){
        ArrayList<Integer> list = new ArrayList<>();
        for (int i : v) {
            list.add(i);
        }
        return list;
    }

    public int knightDialer(int n) {
        int dp[][]=new int[n][10];
        Arrays.fill(dp[0],1);
        List<Integer>[] lists=new List[10];
        lists[0]=asList(4,6);
        lists[1]=asList(8,6);
        lists[2]=asList(7,9);
        lists[3]=asList(4,8);
        lists[4]=asList(3,9);
        lists[5]=asList();
        lists[6]=asList(1,7);
        lists[7]=asList(2,6);
        lists[8]=asList(1,3);
        lists[9]=asList(4,2);
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < 9; j++) {
                int t=0;
                for (Integer integer : lists[j]) {
                    t+=dp[i-1][integer];
                    t%=mod;
                }
                dp[i][j]=t;
            }
        }
        int res=0;
        for (int i : dp[n - 1]) {
            res+=i;
            res%=mod;
        }
        return res;
    }

    public static void main(String[] args) throws IOException {
        System.out.println("1/2/4".replaceAll("/", "-"));
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

class Stack<T> {
    Object[] list;
    int n = 0;

    public Stack(int size) {
        list = new Object[size];
    }

    public void push(T value) {
        list[n++] = value;
    }

    public T pop() {
        return (T) list[--n];
    }

    public boolean isNotEmpty() {
        return n > 0;
    }

    public T peek() {
        return (T) list[n - 1];
    }
}

class PrefixTree {
    int cost = Integer.MAX_VALUE;
    Map<Character, PrefixTree> children;

    PrefixTree() {
        children = new HashMap<>();
    }

    public void insert(String s, int cost) {
        PrefixTree p = this;
        for (int i = 0; i < s.length(); i++) {
            char key = s.charAt(i);
            p = p.children.computeIfAbsent(key, k -> new PrefixTree());

        }
        p.cost = Math.min(p.cost, cost);
    }
}