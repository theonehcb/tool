package com.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/5/31
 **/
@Data
public class AlarmReportNew {
    @ExcelIgnore
    String cellName;
    @ExcelProperty("受影响小区名称")
    String affectedCommunities;
    @ExcelProperty("流水号")
    String orderId;
    @ExcelProperty("工单状态")
    String state;
    @ExcelProperty("建单时间")
    LocalDateTime orderCreateTime;
    @ExcelProperty("关单时间")
    String orderEndTime;
    @ExcelProperty("网元名称")
    String netName;
    @ExcelProperty("维护部门")
    String maintenanceDepartment;
    @ExcelProperty("网络名称")
    String networkName;
    @ExcelProperty("告警名称")
    String alarmName;
    @ExcelProperty("故障处理情况")
    String handlingSituation;
    @ExcelProperty("告警流水号")
    String serialNumber;
    @ExcelProperty("告警描述")
    String alarmDescription;
    @ExcelProperty("告警标题")
    String title;
    @ExcelProperty("故障发生时间")
    LocalDateTime faultOccurrenceTime;
    @ExcelProperty("告警清除时间")
    String alarmClearingTime;
}
