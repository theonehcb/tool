package com.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

import javax.annotation.Resource;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/5/10
 **/
@Data
public class AvgWeak {
    @ExcelProperty("sn")
    String sn;
    @ExcelProperty("次数")
    int n;
    @ExcelProperty("总和")
    double pow;
    @ExcelProperty("平均值")
    double avg;
}
