package com.pojo;


import lombok.Data;


@Data
public class CellDimension<T> {

    Long id;
    String name;
    String cellId;
    String company;
    int breakNum;
    double target;
    int breakNum9;
    int breakNum10;
    int breakNum11;
    int breakNum12;
    int rate9;
    int rate10;
    int rate11;
    int rate12;
    String upToStandard9;
    String upToStandard10;
    String upToStandard11;
    String upToStandard12;
}