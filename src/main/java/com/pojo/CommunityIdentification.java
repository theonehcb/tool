package com.pojo;

import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/3/4
 **/
@Data
public class CommunityIdentification {
    String cellName;
    String remark;
}
