package com.pojo;

import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/11/30
 **/
@Data
public class DailyReport {
    String today;
    Integer vvipNum;
    Integer complainNum;
    String complainMsg;
//    卡顿
    Integer lagNum;
    Integer boxNum;
    Integer userNum;
    Integer watchNum;
    Integer saBadNum;
    Integer saVvipNum;
    Integer saUserNum;
    Integer badAppNum;
    Integer badAppUserNum;
    Integer homeBadNum;
    Integer homeBadUserNum;
    Integer contentBadNum;
    Integer contentBadUserNum;
    Integer homeContentBadNum;
    Integer homeContentBadUserNum;
    Integer badAppTypeNum;
    Integer badAppTop4;
    Integer badIpNum;
    Integer outOfChinaIpNum;
    Integer chinaIpNum;
    Integer provinceIpNum;
    Integer outOfProvinceIpNum;
    Integer lightPathNum;
    String lightPathMsg;
    Integer lightCatNum;
    String lightCatMsg;
    Integer frequentOnlineAndOfflineNum;
    String frequentOnlineAndOfflineMsg;
    Integer oltNum;
    String oltMsg;
    String message;
}
