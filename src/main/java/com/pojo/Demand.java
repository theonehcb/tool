package com.pojo;

import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/14
 **/
@Data
public class Demand {
    String number;
    String periodsNum;
    String reportTime;
    String requirementNum;
    String reportProvince;
    String director;
    String directorPhone;
    String reportor;
    String reportorPhone;
    String contractName;
    String contractCode;
    String customer;
    String industry;
    String proName;
    String introduction;
    String isLeader;
    String contacts;
    String customerPhone;
    String requireArea;
    String city;
    String area;
    String address;
    String isDeploy;
    String deployNum;
    String deployTime;
    String isUrgent;
    String taskStatus;
}
