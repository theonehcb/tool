package com.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/14
 **/
@Data
public class Demand2023 {

        @ExcelIgnore
        String id;

        // 批次
        @ExcelProperty("批次")
        private String batch;

        // 需求上报省
        @ExcelProperty("需求上报省")
        private String reportingProvince;

        // 省公司负责人
        @ExcelProperty("省公司负责人")
        private String provincialManager;

        // 省负责人电话
        @ExcelProperty("省负责人电话")
        private String provincialManagerPhone;

        // 需求上报人
        @ExcelProperty("需求上报人")
        private String reporter;

        // 需求上报人电话
        @ExcelProperty("需求上报人电话")
        private String reporterPhone;

        // 合同名称
        @ExcelProperty("合同名称")
        private String contractName;

        // 客户名称
        @ExcelProperty("客户名称")
        private String customerName;

        // 所属行业
        @ExcelProperty("所属行业")
        private String industry;

        // 项目名称
        @ExcelProperty("项目名称")
        private String projectName;

        // 业务发展模式简介
        @ExcelProperty("业务发展模式简介")
        private String businessModelDescription;

        // 是否龙头客户
        @ExcelProperty("是否龙头客户")
        private boolean isLeadingCustomer;

        // 客户联系人
        @ExcelProperty("客户联系人")
        private String customerContact;

        // 客户联系电话
        @ExcelProperty("客户联系电话")
        private String customerPhone;

        // 需求区域（省区市）
        @ExcelProperty("需求区域（省区市）")
        private String demandRegionProvinceCity;

        // 需求区域（地市州）
        @ExcelProperty("需求区域（地市州）")
        private String demandRegionPrefectureCity;

        // 需求区域（区县）
        @ExcelProperty("需求区域（区县）")
        private String demandRegionDistrictCounty;

        // 需求所在具体点位地址（城区精确到小区，县及乡镇精确到村或具体位置如某县政府大楼、某购物中心、某办公楼、某住宅小区等）
        @ExcelProperty("需求所在具体点位地址（城区精确到小区，县及乡镇精确到村或具体位置如某县政府大楼、某购物中心、某办公楼、某住宅小区等）")
        private String demandLocationDetail;
}
