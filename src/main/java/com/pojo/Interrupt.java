package com.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/30
 **/
@Data
public class Interrupt {
    @ExcelProperty("interruptionTime")
    int interruptionTime;
    @ExcelProperty("interruptionCnt")
    int interruptionCnt;
    @ExcelProperty("OLTIP")
    String ip;
    @ExcelProperty("OLTPORT")
    String pon;

    @ExcelIgnore
    String ipPon;
    @ExcelProperty("日期")

    LocalDateTime date;
    @ExcelProperty("小区名称")
    String cellName;

}
