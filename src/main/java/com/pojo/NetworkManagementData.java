package com.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/3
 **/
@Data
public class NetworkManagementData {


    @ExcelProperty("OLTPON")
    private String oltPon;

    @ExcelProperty("SN")
    private String serialNumber;
    @ExcelProperty("小区")
    String cell;

    @ExcelProperty("接收光功率(dBm)")
    private Double receiveOpticalPowerDbm;


}
