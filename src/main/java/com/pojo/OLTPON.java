package com.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/2/21
 **/
@Data
public class OLTPON {
    @ExcelProperty("OLTPON")
    String ipPon;
    @ExcelProperty("所属小区")
    String cellName;
}
