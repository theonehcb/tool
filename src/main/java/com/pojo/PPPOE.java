package com.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/26
 **/
@Data
public class PPPOE{
    @ExcelProperty("账号")
    String account;
    @ExcelProperty("sn")
    String sn;
    @ExcelProperty("pon")
    String ipPon;
    @ExcelProperty("上线时间")
    String onlineTime;
}

