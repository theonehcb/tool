package com.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/12/8
 **/
@Data
public class Parameter {
    @ExcelIgnore
    double latitude;
    @ExcelIgnore
    double longitude;

    @ExcelProperty("province")
    String province;
    @ExcelProperty("city")
    String city;
    @ExcelProperty("gNB_id_16jz")
    String gnbId16;
    @ExcelProperty("cell_id_16jz")
    String cellId16;
    @ExcelProperty("id50m")
    String id50m;
    @ExcelProperty("gNB_id")
    String gnbId;
    @ExcelProperty("CGI")
    String cgi;

    @ExcelProperty("base_station_name")
    String stationName;

    @ExcelProperty("type")
    String state;//是否开通
    @ExcelProperty("site_type")//4tr
    String siteType;
    @ExcelProperty("base_station_type")
    String type;//宏站、室分
    @ExcelProperty("frequency_band")
    String frequencyBand;
    @ExcelProperty("project_time")
    String projectTime;//期数
    @ExcelIgnore
    String G5;//5高类型
    @ExcelIgnore
    int DDD;
}
