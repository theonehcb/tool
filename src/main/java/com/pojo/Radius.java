package com.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/25
 **/
@Data
public class Radius {

    @ExcelProperty("用户账号")
    private String account;
    @ExcelProperty("上线时间")
    String onlineTime;
    @ExcelProperty("下线时间")
    String offlineTime;

    @ExcelProperty("VLAN信息")
    private String vlanInfo;

    @ExcelIgnore
    String ipPon;
    @ExcelIgnore
    String ontId;
}
