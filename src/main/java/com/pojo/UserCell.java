package com.pojo;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/2/5
 **/
@Data
public class UserCell {
   String account;
   String sn;
   String cell;
}
