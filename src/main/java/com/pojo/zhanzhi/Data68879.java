package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/22
 **/
@Data
@TableName("\"68879\"")
public class Data68879 {
    String kpiCol3;
    String kpiCol2;
    String kpiCol1;
    @TableField("is_700m")
    String is700m;
    String overlayCode;
    String cell;
    String tac;
    String provName;
    String statisYmd;
}
