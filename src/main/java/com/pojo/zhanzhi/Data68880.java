package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Value;

/**
 * //Description: TODO
 * //Author: hechaobo
 * //Date: 2024/4/22
 **/
@Data
@TableName("\"68880\"")
public class Data68880 {
    String kpiCol32;
    String kpiCol31;
    String kpiCol30;
    String kpiCol29;
    String kpiCol28;
    String kpiCol27;
    String kpiCol26;
    String kpiCol25;
    String kpiCol24;
    String kpiCol23;
    String kpiCol22;
    String kpiCol21;
    String kpiCol20;
    String kpiCol19;
    //CsvBindByName(column="kpi_col18")
    String kpiCol18;
    //CsvBindByName(column="kpi_col17")
    String kpiCol17;
    //CsvBindByName(column="kpi_col16")
    String kpiCol16;
    //CsvBindByName(column="kpi_col15")
    String kpiCol15;
    //CsvBindByName(column="kpi_col14")
    String kpiCol14;
    //CsvBindByName(column="kpi_col13")
    String kpiCol13;
    //CsvBindByName(column="kpi_col12")
    String kpiCol12;
    //CsvBindByName(column="kpi_col11")
    String kpiCol11;
    //CsvBindByName(column="kpi_col10")
    String kpiCol10;
    //CsvBindByName(column="kpi_col9")
    String kpiCol9;
    //CsvBindByName(column="kpi_col8")
    String kpiCol8;
    //CsvBindByName(column="kpi_col7")
    String kpiCol7;
    //CsvBindByName(column="kpi_col6")
    String kpiCol6;
    //CsvBindByName(column="kpi_col5")
    String kpiCol5;
    //CsvBindByName(column="kpi_col4")
    String kpiCol4;
    //CsvBindByName(column="kpi_col3")
    String kpiCol3;
    //CsvBindByName(column="kpi_col2")
    String kpiCol2;
    //CsvBindByName(column="kpi_col1")
    String kpiCol1;
    //CsvBindByName(column="prov_name")
    String provName;
    //CsvBindByName(column="statis_ymd")
    String statisYmd;
}
