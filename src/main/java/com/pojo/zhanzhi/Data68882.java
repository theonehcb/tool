package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/22
 **/
@Data
@TableName("\"68882\"")
public class Data68882 {
   //vBindByName(column = "g5_term_user_flux_4g")
    @TableField("g5_term_user_flux_4g")
    String g5TermUserFlux4g;
   //vBindByName(column = "g5_term_user_flux_5g")
    @TableField("g5_term_user_flux_5g")
    String g5TermUserFlux5g;
   //vBindByName(column = "g5_term_user_cnt_sa")
    String g5TermUserCntSa;
   //vBindByName(column = "g5_term_user_cnt_switch")
    String g5TermUserCntSwitch;
   //vBindByName(column = "g5_term_user_cnt")
    String g5TermUserCnt;
   //vBindByName(column = "overlay_code")
    String overlayCode;
   //vBindByName(column = "prov_name")
    String provName;
   //vBindByName(column = "statis_ymd")
    String statisYmd;

}
