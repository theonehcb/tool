package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/11
 **/
@Data
@TableName("\"68916\"")
public class Data68916 {
   //vBindByName(column="prov")
    String prov;
   //vBindByName(column="grid")
    String grid;
   //vBindByName(column="tac")
    String tac;
   //vBindByName(column="sa_user_cnt")
    String saUserCnt;
   //vBindByName(column="sa_smf_user_cnt")
    String saSmfUserCnt;
    @TableField("sa_smf_5g_dura")
   //vBindByName(column="sa_smf_5g_dura")
    String saSmf5gDura;
    @TableField("sa_smf_4g_dura")
   //vBindByName(column="sa_smf_4g_dura")
    String saSmf4gDura;
   //vBindByName(column="dura_stay_ratio")
    String duraStayRatio;
   //vBindByName(column="sa_smf_5g_flux")
    @TableField("sa_smf_5g_flux")
    String saSmf5gFlux;
   //vBindByName(column="sa_smf_4g_flux")
    @TableField("sa_smf_4g_flux")
    String saSmf4gFlux;
   //vBindByName(column="flux_stay_ratio")
    String fluxStayRatio;
   //vBindByName(column="prov_id")
    String provId;
   //vBindByName(column="statis_ymd")
    String statisYmd;
}
