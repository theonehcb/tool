package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/22
 **/
@Data
@TableName("\"68918\"")
public class Data68918 {
    String provName;
    @TableField("term_5g_flux_5g")
    String term5gFlux5g;
    @TableField("term_4g_flux_4g")
    String term5gFlux4g;
    @TableField("ntw_5g_flux_5g")
    String ntw5gFlux5g;
    @TableField("ntw_5g_flux_4g")
    String ntw5gFlux4g;
    @TableField("term_5g_unitntw_flux_ratio")
    String term5gUnitntwFluxRatio;
    @TableField("ntw_5g_term_5g_flux_ratio")
    String ntw5gTerm5gFluxRatio;
    String provId;
    String statisYmd;
    @TableField("term_4g_flux_4g")
    String term4gFlux4g;
}
