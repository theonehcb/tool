package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/22
 **/
@Data
@TableName("\"68919\"")
public class Data68919 {
   //vBindByName(column = "prov_name")
    String provName;
   //vBindByName(column = "cell_nbr")
    String cellNbr;
   //vBindByName(column = "flux_judcon")
    String fluxJudcon;
   //vBindByName(column = "g5_term_user_cnt")
    String g5TermUserCnt;
   //vBindByName(column = "g5_term_user_cnt_is_700m")
    @TableField("g5_term_user_cnt_is_700m")
    String g5TermUserCntIs700m;
   //vBindByName(column = "statis_ymd")
    String statisYmd;
}
