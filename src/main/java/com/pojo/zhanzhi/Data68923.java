package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/22
 **/
@Data
@TableName("\"68923\"")
public class Data68923 {
   //vBindByName(column = "prov_code")
    String provCode;
   //vBindByName(column = "city_code")
    String cityCode;
   //vBindByName(column = "area_type")
    String areaType;
   //vBindByName(column = "user_cnt_5g")
    @TableField("user_cnt_5g")
    String userCnt5g;
   //vBindByName(column = "flux_5g")
    @TableField("flux_5g")
    String flux5g;
   //vBindByName(column = "net_user_cnt_700m")
    @TableField("net_user_cnt_700m")
    String netUserCnt700m;
   //vBindByName(column = "flux_net_700m")
    @TableField("flux_net_700m")
    String fluxNet700m;
   //vBindByName(column = "statis_ymd")
    String statisYmd;
   //vBindByName(column = "prov_id")
    String provId;
}
