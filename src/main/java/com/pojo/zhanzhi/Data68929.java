package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/11
 **/
@Data
@TableName("\"68929\"")
public class Data68929 {
   //vBindByName(column="prov_name")
    String provName;
   //vBindByName(column="city_name")
    String cityName;
   //vBindByName(column="cell_code")
    String cellCode;
   //vBindByName(column="user_cnt")
    String userCnt;
   //vBindByName(column="tot_call_dura_sec")
    String totCallDuraSec;
   //vBindByName(column="tot_flux")
    String totFlux;
   //vBindByName(column="statis_ymd")
    String statisYmd;
   //vBindByName(column="prov_id")
    String provId;
}
