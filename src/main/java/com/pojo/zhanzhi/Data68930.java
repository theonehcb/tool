package com.pojo.zhanzhi;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/11
 **/
@Data
@TableName("\"68930\"")
public class Data68930 {
   //vBindByName(column = "dimen_col")
    String dimenCol;
   //vBindByName(column = "rpt_code")
    String rptCode;
   //vBindByName(column = "sa_user_cnt")
    String saUserCnt;
   //vBindByName(column = "sa_smf_user_cnt")
    String saSmfUserCnt;
   //vBindByName(column = "sa_smf_5g_dura")
    @TableField("sa_smf_5g_dura")

    String saSmf5gDura;
   //vBindByName(column = "sa_smf_5g_flux")
    @TableField("sa_smf_5g_flux")

    String saSmf5gFlux;
   //vBindByName(column = "sa_smf_4g_dura")
    @TableField("sa_smf_4g_dura")

    String saSmf4gDura;
   //vBindByName(column = "sa_smf_4g_flux")
    @TableField("sa_smf_4g_flux")
    String saSmf4gFlux;
   //vBindByName(column = "statis_ymd")
    String statisYmd;
   //vBindByName(column = "prov_id")
    String provId;
   //vBindByName(column = "tb_name")
    String tbName;
}
