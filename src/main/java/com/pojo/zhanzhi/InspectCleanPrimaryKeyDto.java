package com.pojo.zhanzhi;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;
@Data
public class InspectCleanPrimaryKeyDto {

    //系统名称
    private String systemName;
    //表名
    private String tableName;
    private String benchSystemName;
    private String benchTableName;
    //文件名
    private String fileName;
    //主键数据信息
    private String primaryKeyInfo;
    //sheet页名
    private String sheetName;
    //行号
    private Integer lineNumber;
    //第一行的字段信息
    private Map<String, Integer> firstLineMap;
    //存字段信息<创建时间，数据><更新时间，数据>
    private Map<String, String> fieldMap = new HashMap<>();
    //map<哪一个字段没对上，字段列索引>
    private Map<String, Integer> noMatch = new HashMap<>();
    //该行数据是否被稽核到（默认是0没有稽核，1是稽核）
    private Integer isMatchLine = 0;
    //该行数据是否全部稽核成功(默认0是有核失败，1是全部稽核成功)
    private Integer isMatchLineSucess = 0;
}
