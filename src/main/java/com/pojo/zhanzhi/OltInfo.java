package com.pojo.zhanzhi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/6/12
 **/
@Data
public class OltInfo {
    @ExcelProperty("room")
    String room;
    @ExcelProperty("olt_name")
    String olt;
}
