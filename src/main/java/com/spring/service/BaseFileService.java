package com.spring.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.util.excel.FileUtilH;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @Description: 需要读取文件的service
 * @Author: hechaobo
 * @Date: 2024/3/11
 **/
@Slf4j
public  class BaseFileService <M extends BaseMapper<T>, T> extends ServiceImpl<M, T>{
    public static Map<String,BaseFileService> map=new HashMap<>();

    /**
     * 通过文件类型获取入库、校验逻辑
     * @param type
     */
    public void put(String type){
        map.put(type,this);
    }

    /**
     * 默认入库方式
     * @param path
     */
    public  void save(String path){
        List<T> list=new LinkedList<>();
        FileUtilH.read(path,getEntityClass(), t->{
            list.add(t);
            if(list.size()==1000){
                saveBatch(list);
                list.clear();
            }
        });
        saveBatch(list);
        log.info("入库成功");
    };

    /**
     * 默认校验
     * @param path
     */
    public void check(String path){
        FileUtilH.validate(path, getEntityClass());
        log.info("校验成功，{}",this.getClass().getSimpleName());
    }
}
