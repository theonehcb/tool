package com.test;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/30
 **/
@Data
public class Alarm {
    @ExcelProperty("account")
    String account;
    @ExcelProperty("offline_time")
    String offlineTime;
    @ExcelProperty("olt_ip")
    String oltip;
    @ExcelProperty("pon")
    String pon;
    @ExcelProperty("happen_time")
    Date happenTime;
    @ExcelProperty("clear_time")
    Date clearTime;
}
