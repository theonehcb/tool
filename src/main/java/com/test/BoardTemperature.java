package com.test;

import com.alibaba.excel.annotation.ExcelProperty;

import java.util.Date;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/18
 **/
public class BoardTemperature {

    long id;

    @ExcelProperty("入库时间")
    private Date entryTime;

    @ExcelProperty("厂商")
    private String manufacturer;

    @ExcelProperty("网元IP")
    private String neIp;

    @ExcelProperty("网元类型")
    private String neType;

    @ExcelProperty("板卡种类")
    private String boardCategory;

    @ExcelProperty("业务类型")
    private String businessType;

    @ExcelProperty("板卡型号")
    private String boardModel;

    @ExcelProperty("板卡名称")
    private String boardName;

    @ExcelProperty("板卡描述")
    private String boardDescription;

    @ExcelProperty("槽位")
    private String slot;

    @ExcelProperty("板卡在线状态")
    private String boardOnlineStatus;

    @ExcelProperty("统计端口总数")
    private int totalPortCount;

    @ExcelProperty("温度(°C)")
    private double temperature;

    @ExcelProperty("CPU占用率(%)")
    private double cpuUsage;

    @ExcelProperty("内存占用率(%)")
    private double memoryUsage;

    String slotNum;;
}
