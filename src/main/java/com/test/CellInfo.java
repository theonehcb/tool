package com.test;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: 小区信息
 * @Author: hechaobo
 * @Date: 2023/12/18
 **/
@Data
public class CellInfo {
    @ExcelProperty("中文名称")
    String cellName;
    @ExcelProperty("中心经度")
    double longitude;
    @ExcelProperty("中心纬度")
    double latitude;

    @ExcelProperty("网元内部编码")
    private String internalCode;

    @ExcelProperty("是否合格")
    private String qualified;

    @ExcelProperty("别名")
    private String alias;

    @ExcelProperty("分级简称")
    private String abbreviatedLevel;

    @ExcelProperty("小区标准命名")
    private String standardizedNaming;

    @ExcelProperty("地址类型")
    private String addressType;

    @ExcelProperty("所属地市")
    private String city;

    @ExcelProperty("所属区县")
    private String district;

    @ExcelProperty("所属乡镇/街道")
    private String townOrStreet;

    @ExcelProperty("所属路/巷/行政村")
    private String roadOrLaneOrVillage;

    @ExcelProperty("小区简称")
    private String abbreviation;

    @ExcelProperty("楼宇数量（幢）")
    private String buildingCount;

    @ExcelProperty("重点小区")
    private String keyNeighborhood;

    @ExcelProperty("户数")
    private String householdCount;

    @ExcelProperty("覆盖类型")
    private String coverageType;

    @ExcelProperty("合作运营商")
    private String cooperatingOperator;

    @ExcelProperty("小区建设模式")
    private String constructionMode;

    @ExcelProperty("主责装维人员")
    private String primaryMaintenancePersonnel;

    @ExcelProperty("主责装维人员姓名")
    private String primaryMaintenancePersonnelName;

    @ExcelProperty("装维组")
    private String maintenanceGroup;

    @ExcelProperty("装维区域")
    private String maintenanceArea;

    @ExcelProperty("装维单位")
    private String maintenanceUnit;

    @ExcelProperty("小区合作模式")
    private String neighborhoodCooperationMode;

    @ExcelProperty("社区归属网格")
    private String communityGridAffiliation;

    @ExcelProperty("营销品牌")
    private String marketingBrand;

    @ExcelProperty("千兆支持情况")
    private String gigabitSupportStatus;

    @ExcelProperty("验收状态")
    private String inspectionStatus;

    @ExcelProperty("验收时间")
    private String inspectionTime;

    @ExcelProperty("首次可营销时间")
    private String firstMarketingTime;

    @ExcelProperty("小区GIS边界ID")
    private String neighborhoodGISBoundaryId;

    @ExcelProperty("所属分公司")
    private String subsidiaryCompany;

    @ExcelProperty("备注")
    private String remark;

    @ExcelProperty("数据维护部门")
    private String dataMaintenanceDepartment;

    @ExcelProperty("时间戳")
    private String timestamp;

    @ExcelProperty("创建人")
    private String creator;

    @ExcelProperty("创建时间")
    private String creationTime;

    @ExcelProperty("修改时间")
    private String modificationTime;

    @ExcelProperty("修改人")
    private String modifier;
}
