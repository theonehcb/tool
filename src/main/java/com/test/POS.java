package com.test;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/8/14
 **/
@Data
public class POS extends Alarm {
    @ExcelProperty(index = 2)
    String cell;
    @ExcelProperty(index = 3)
    String olt;
    @ExcelProperty(index = 5)
    String pon;
}
