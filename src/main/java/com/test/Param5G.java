package com.test;

import com.alibaba.excel.annotation.ExcelProperty;

import java.time.LocalDateTime;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/23
 **/
public class Param5G {
    long id;
    @ExcelProperty("省份")
    String province;
    @ExcelProperty("地市")
    String city;
    @ExcelProperty("县城")
    String county;
    String cover;
    int siteId;
    String siteType;
    double longitude;
    double latitude;
    boolean redcap;
    LocalDateTime time;
    //经纬度编码
    long gisCode;
}
