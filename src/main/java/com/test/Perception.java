package com.test;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/17
 **/
@Data
public class Perception {
    long id;
    String cellName;
    @ExcelProperty("上网速度")
    private Double internetSpeed;

    @ExcelProperty("网络连接稳定性")
    private Double networkConnectivityStability;

    @ExcelProperty("玩游戏")
    private Double gamingExperience;

    @ExcelProperty("看视频")
    private Double videoStreamingExperience;

    @ExcelProperty("互联网电视")
    private Double internetTvExperience;

    @ExcelProperty("浏览网页")
    private Double webBrowsingExperience;

    @ExcelProperty("装机H5满意度")
    private Double installationH5SatisfactionScore;

    @ExcelProperty("H5维修满意度得分")
    private Double repairH5SatisfactionScore;

    private Double Q2;
    private Double Q5;
}
