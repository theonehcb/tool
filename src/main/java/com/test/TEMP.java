package com.test;

import lombok.Data;

@Data
public class TEMP {
    double lon;
    double lat;
    long code;
    String name;
    String scenes;
}