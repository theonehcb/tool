package com.util.ai;

import cn.hutool.core.io.FileUtil;
import com.alibaba.dashscope.aigc.conversation.Conversation;
import com.alibaba.dashscope.aigc.conversation.ConversationParam;
import com.alibaba.dashscope.aigc.conversation.ConversationResult;
import com.alibaba.dashscope.utils.Constants;

import java.io.BufferedWriter;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/11
 **/
public class demo {
    public static void quickStart()  throws Exception{

        Constants.apiKey="sk-dfdb1cfd750c49678d6520fc43db2362";
        Conversation conversation = new Conversation();
        String prompt = "请根据以下功能点，给出总计90条前端、后端的工作内容以及5个代码片段，并尽可能贴近以下功能点：\n" +
                "测试需求管理-提交\n" +
                "测试需求管理-关闭\n" +
                "需求测试目标地图加载功能\n" +
                "需求测试目标工单号码查询功能\n" +
                "需求测试目标需求名称查询功能\n" +
                "需求测试目标截止时间查询功能\n" +
                "需求测试目标创建日期查询功能\n" +
                "需求测试目标当前状态查询功能\n" +
                "需求测试目标当前机构查询功能\n" +
                "需求测试目标网格列表查询功能\n" +
                "需求测试目标网格项点击查询功能\n" +
                "需求测试目标网格新增\n" +
                "需求测试目标网格编辑提交\n" +
                "需求测试目标网格删除";
        ConversationParam param = ConversationParam
                .builder()
                .model(Conversation.Models.QWEN_TURBO)
                .prompt(prompt)
                .build();
        ConversationResult result = conversation.call(param);
        String text = result.getOutput().getText();
        BufferedWriter writer = FileUtil.getWriter("/temp/t.txt", "utf-8", false);
        writer.write(text);
        writer.close();
    }

    public static void main(String[] args) throws Exception {
        quickStart();
    }
}
