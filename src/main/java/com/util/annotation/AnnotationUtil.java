package com.util.annotation;

import javax.validation.constraints.NotBlank;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/10/26
 **/
public class AnnotationUtil {
    public static List<Field> getFiledByAnnotation(Object o, Class annotation){
        Field[] declaredFields = o.getClass().getDeclaredFields();
        List<Field> fields = new ArrayList<Field>();
        for (Field declaredField : declaredFields) {
            if(declaredField.getAnnotation(annotation)!=null){
                fields.add(declaredField);
            }
        }
        return fields;
    }
}
