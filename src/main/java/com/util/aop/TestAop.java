package com.util.aop;

import org.springframework.stereotype.Component;


/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/4
 **/
@Component
public class TestAop {
    @Timeing
    public void test() throws InterruptedException {
        Thread.sleep(1000);
    }
}
