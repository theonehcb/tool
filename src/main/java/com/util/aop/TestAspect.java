package com.util.aop;

import cn.hutool.core.date.TimeInterval;
import org.apache.poi.ss.formula.functions.T;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/4
 **/
@Aspect
@Component
public class TestAspect {
    @Pointcut("@annotation(com.util.aop.Timeing)")
    public void pointcut(){}
    @Around(value = "pointcut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println(joinPoint.getKind());
        TimeInterval timeInterval=new TimeInterval();
        timeInterval.start();
        joinPoint.proceed();
        System.out.println(timeInterval.interval());
    }
}