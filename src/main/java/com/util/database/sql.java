package com.util.database;

import com.pojo.AlarmReportNew;
import com.test.CellInfo;
import com.test.Param5G;

import java.lang.reflect.Field;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/10/13
 **/
public class sql {
    public static void main(String[] args) {
      generate();
    }
    public static void generate(){
        getmysql(AlarmReportNew.class);
    }
    public static void getpgsql(Class cls){
        String tableName = getName(cls.getSimpleName()).substring(1);
        System.out.println("DROP TABLE IF EXISTS \"public\"."+tableName+";");
        System.out.println("CREATE TABLE \"public\"."+tableName+"(");
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            String name = declaredField.getName();

           StringBuilder sb=getName(name);
            String typeName = declaredField.getGenericType().getTypeName();
            if(typeName.equals("java.lang.Double")||typeName.equals("double")){
                System.out.println("\""+sb+"\""+" float8,");
            }
            else if(typeName.equals("java.util.Date")){
                System.out.println("\""+sb+"\""+" timestamp(0),");
            }
            else if(typeName.equals("java.lang.Integer")||typeName.equals("int")){
                System.out.println("\""+sb+"\""+" int4,");
            }else {
                System.out.println("\""+sb+"\""+" varchar(255) COLLATE \"pg_catalog\".\"default\",");
            }
        }
        System.out.println(");");
    }
    public static void getmysql(Class cls){
        System.out.println("SET NAMES utf8mb4;\n" +
                "SET FOREIGN_KEY_CHECKS = 0;");
        System.out.println("CREATE TABLE `"+getName(cls.getSimpleName()).substring(1)+"`(");
        Field[] declaredFields = cls.getDeclaredFields();
        declaredFields[0].setAccessible(true);
        StringBuilder id = getName(declaredFields[0].getName());
        System.out.println("`"+id+"` int(0) NOT NULL AUTO_INCREMENT,");
        for (int j=1;j<declaredFields.length;j++){
            Field declaredField=declaredFields[j];
            String name = declaredField.getName();
            StringBuilder sb = getName(name);
            String typeName = declaredField.getGenericType().getTypeName();
            if(typeName.equals("java.lang.Integer")||typeName.equals("int")){
                System.out.println("`"+sb+"`"+"int(0) NULL DEFAULT NULL,");
            }
            else if(typeName.equals("java.util.Date")||typeName.equals("java.time.LocalDateTime")){
                System.out.println("`"+sb+"`"+"datetime(0) NULL DEFAULT NULL,");
            }
            else if(typeName.equals("double")||typeName.equals("java.lang.Double")){
                System.out.println("`"+sb+"`"+"double(0, 0) NULL DEFAULT NULL,");
            }
            else {
                System.out.println("`"+sb+"`"+"varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,");
            }
        }
        System.out.println("PRIMARY KEY (`"+id+"`) USING BTREE");
        System.out.println(")ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;");
        System.out.println("SET FOREIGN_KEY_CHECKS = 1;");
    }

    public static StringBuilder getName(String name) {
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<name.length();i++){
            if(name.charAt(i)<'a'&&name.charAt(i)>='A'){
                sb.append('_');
                sb.append((char)(name.charAt(i)+32));
            }
            else {
                sb.append(name.charAt(i));
            }
        }
        return sb;
    }
}
