package com.util.excel;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.DataFormatData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.RowWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

import java.awt.Color;
import java.util.List;

/**
 * @Description: 设置一种每行交叉显示颜色的格式
 * @Author: hechaobo
 * @Date: 2024/3/7
 **/
public class BeautifulHandler implements CellWriteHandler{
    CellStyle cellStyle1;
    CellStyle cellStyle2;
    //表头样式
    CellStyle headCellStyle;
    Workbook workbook;

    @Override
    public int order() {
        return 1000;
    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
        WriteCellData<?> writeCellData = cellDataList.get(0);
        WriteCellStyle orCreateStyle = writeCellData.getOrCreateStyle();
        orCreateStyle.setFillForegroundColor((short) (relativeRowIndex%2+42));
        orCreateStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
    }

    /**
     * 百分数格式
     *
     * @param cellStyle
     */
    public  static void addPercentStyle(CellStyle cellStyle,Workbook workbook) {
        cellStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
    }

    /**
     * 设置指定颜色
     */
    public static void setColor(CellStyle cellStyle, Color color) {
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        ((XSSFCellStyle) cellStyle).setFillForegroundColor(new XSSFColor(color, null));
    }

    /**
     * 换行
     *
     * @param cellStyle
     */
    public void lineBreak(CellStyle cellStyle) {
        cellStyle.setWrapText(true);
    }

    public CellStyle getHeadStyle() {
        if (headCellStyle != null) return headCellStyle;
        headCellStyle = getBaseStyle(workbook);
        //设置颜色
        setColor(headCellStyle, new Color(98, 166, 168));
        //设置加粗
        Font font = workbook.createFont();
        font.setBold(true);
        headCellStyle.setFont(font);
        return headCellStyle;
    }

    public CellStyle getCellStyle1() {
        if (cellStyle1 != null) return cellStyle1;
        cellStyle1 = getBaseStyle(workbook);
        //设置颜色
        setColor(cellStyle1, new Color(176, 241, 239));
        return cellStyle1;
    }

    public CellStyle getCellStyle2() {
        if (cellStyle2 != null) return cellStyle2;
        cellStyle2 = getBaseStyle(workbook);
        return cellStyle2;
    }

    /**
     * 获取基本样式
     * 文字居中
     *
     * @return
     */
    public static CellStyle getBaseStyle(Workbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        return cellStyle;
    }
}
