package com.util.excel;

import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.function.Function;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/12/28
 **/
public class ColCellStyle implements CellWriteHandler {

    Function<Workbook, CellStyle> consumer;
    int col;

    public ColCellStyle(int col,Function<Workbook,CellStyle> consumer){
        this.col=col;
        this.consumer=consumer;
    }
    @Override
    public void afterCellDispose(CellWriteHandlerContext context) {
        if(context.getColumnIndex()==col){
            CellStyle apply = consumer.apply(context.getWriteWorkbookHolder().getWorkbook());
            context.getCell().setCellStyle(apply);
            System.out.println(1);
        }
    }
}
