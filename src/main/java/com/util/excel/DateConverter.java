package com.util.excel;

import cn.hutool.core.date.DateUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/10
 **/
public class DateConverter implements Converter<LocalDateTime> {


    @Override
    public LocalDateTime convertToJavaData(ReadConverterContext<?> context) throws Exception {
        String stringValue = context.getReadCellData().getStringValue();
        if(stringValue==null)return null;
        return LocalDateTime.parse(DateUtil.parse(stringValue).toString(),DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
