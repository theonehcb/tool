package com.util.excel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/4/18
 **/
public class DoubleConverter implements Converter<Double> {
    @Override
    public Double convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String stringValue = cellData.getStringValue();
        double v = 0;
        try {
            v = Double.parseDouble(stringValue);
        } catch (NumberFormatException e) {
        }
        return v;
    }
}
