package com.util.excel;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/9/22
 **/
public class NoFieldException extends RuntimeException{
    public NoFieldException(String s){
        super(s);
    }
}
