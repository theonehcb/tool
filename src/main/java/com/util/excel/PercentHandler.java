package com.util.excel;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.DataFormatData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Description: 为指定列设置百分比格式
 * @Author: hechaobo
 * @Date: 2024/5/10
 **/
public class PercentHandler implements CellWriteHandler {
    Set<Integer> set = new HashSet<>();

    public PercentHandler(int... col) {
        for (int i : col) {
            set.add(i);
        }
    }
    public PercentHandler(Collection<Integer> col) {
        set.addAll(col);
    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {

        if(set.contains(cell.getColumnIndex())){
            WriteCellStyle orCreateStyle = cellDataList.get(0).getOrCreateStyle();
            orCreateStyle.setDataFormatData(new DataFormatData(){{
                setFormat("0.00%");
            }});
        }

    }
}
