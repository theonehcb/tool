package com.util.generator;

import com.Util;
import com.util.database.sql;
import com.pojo.Radius;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/2/23
 **/
public class BatchInsert {
    public static void main(String[] args) {
        List list1=new LinkedList();
        List list2=new LinkedList();
        Field[] declaredFields = Radius.class.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            String name = declaredField.getName();
            list1.add(sql.getName(name).toString());
            list2.add("#{item."+name+"}");
        }
        System.out.println(Util.join(",\n",list1));
        System.out.println(Util.join(",\n",list2));
    }
}
