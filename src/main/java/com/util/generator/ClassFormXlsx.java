package com.util.generator;

import cn.hutool.core.io.FileUtil;
import com.Util;
import com.pojo.AlarmReportNew;
import com.pojo.zhanzhi.*;
import com.util.database.sql;
import com.test.Param5G;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/10/27
 **/
public class ClassFormXlsx {
    static String templatePath="D:\\idea\\tool\\tool\\src\\main\\java\\com\\util\\generator\\";

    public static void main(String[] args) {
//        create("AvgAlarmTime","D:\\家宽\\btth\\module\\data-anh\\src\\main\\java\\com\\xps\\anh\\communityScore");
//        create(Param5G.class,"D:\\nbtest\\console-back\\nb-module\\nb-parameter\\src\\main\\java\\com\\wxxx\\coverquery");
        createXml(AlarmReportNew.class,"D:\\idea\\btth\\module\\data-anh\\src\\main\\java\\com\\xps\\anh\\domain");
    }

    private static void create(String className,String path){

        String[] split = path.split("\\\\");
        int i=0;
        for(;i<split.length;i++){
            if(split[i].equals("java")){
                break;
            }
        }
        String packge="";
        List pp=new LinkedList();
        for(i++;i<split.length;i++){
            pp.add(split[i]);
        }
        packge=Util.join(".",pp);
        String table= sql.getName(className).toString().substring(1);


        //生成mapper
        try {
            List<String> list = Files.readAllLines(Paths.get(templatePath+"mapper"));
            String p=packge;
            List<String> collect = list.stream().map(s -> s.replace("$package", p).
                    replace("$className", className)
                    .replace("$table", table))
                    .collect(Collectors.toList());
            FileUtil.writeLines(collect,path+"\\mapper\\"+className+"Mapper.java","utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //生成service
        try {
            List<String> list = Files.readAllLines(Paths.get(templatePath+"service"));
            String p=packge;
            List<String> collect = list.stream().map(s -> s.replace("$package", p).replace("$className", className)).collect(Collectors.toList());
            FileUtil.writeLines(collect,path+"\\service\\"+className+"Service.java","utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void create(Class cls,String path){
        String className=cls.getSimpleName();
        String packge = getPackge(path);
        //生成pojo
        try {
            List<String> list = Files.readAllLines(Paths.get(templatePath+"pojo"));
            String p=packge;
            List<String> collect = list.stream().map(s -> s.replace("$package", p).replace("$className", className)).collect(Collectors.toList());
            FileUtil.writeLines(collect,path+"\\"+className+".java","utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //生成mapper
        try {
            createMapper(cls,path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //生成service
        createService(cls,path);
        //生成xml
        createXml(cls,path);
    }

    private static String getPackge(String path) {
        String packge="";
        String[] split = path.split("\\\\");
        List pp=new LinkedList();
        int i=0;
        for(;i<split.length;i++){
            if(split[i].equals("java")){
                break;
            }
        }
        for(i++;i<split.length;i++){
            pp.add(split[i]);
        }
        packge=Util.join(".",pp);
        return packge;
    }
    public static void createMapper(Class cls,String path) throws Exception {
        String className = cls.getSimpleName();
        String table= sql.getName(className).toString().substring(1);
        List<String> list = Files.readAllLines(Paths.get(templatePath+"mapper"));
        List<String> collect = list.stream().map(s -> s.replace("$package", getPackge(path)).
                replace("$className", className)
                .replace("$table", table))
                .collect(Collectors.toList());
        FileUtil.writeLines(collect,path+"\\mapper\\"+className+"Mapper.java","utf-8");
    }
    public static void createXml(Class cls,String path){
        List list1=new LinkedList();
        List list2=new LinkedList();
        String className = cls.getSimpleName();
        String table= sql.getName(className).toString().substring(1);
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            String name = declaredField.getName();
            list1.add(sql.getName(name).toString());
            list2.add("#{item."+name+"}");
        }
        String join = Util.join("\n,", list1);
        System.out.println(join);
        String join1 = Util.join("\n,", list2);
        System.out.println(join1);
        try {
            List<String> list = Files.readAllLines(Paths.get(templatePath+"xml"));
            String p=getPackge(path);
            List<String> collect = list.stream().map(s -> s.replace("$package", p+"."+"mapper."+className+"Mapper")
                    .replace("$table", table)
                    .replace("$columnList",join)
                    .replace("$list",join1)).collect(Collectors.toList());
            FileUtil.writeLines(collect,path+"\\mapper\\"+className+"Mapper.xml","utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void createService(Class cls,String path){
        String className = cls.getSimpleName();
        try {
            List<String> list = Files.readAllLines(Paths.get(templatePath+"service"));
            List<String> collect = list.stream().map(s -> s.replace("$package", getPackge(path)).replace("$className", className)).collect(Collectors.toList());
            FileUtil.writeLines(collect,path+"\\service\\"+className+"Service.java","utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
