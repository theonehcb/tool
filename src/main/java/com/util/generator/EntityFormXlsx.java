package com.util.generator;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.util.excel.FileUtilH;
import com.util.excel.Listener;

import java.util.Map;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/10/26
 **/
public class EntityFormXlsx {
    public static void main(String[] args) {
//        print("D:\\temp\\fill.xlsx","字段名称","描述");

        EasyExcel.read("D:\\temp\\t.csv",new Listener(){
            int i=0;
            @Override
            public void invoke(Map<Integer, String> integerStringMap, AnalysisContext analysisContext) {
                super.invoke(integerStringMap, analysisContext);

                    System.out.println("--------------------------------");
                    for (String value : integerStringMap.values()) {
                        System.out.println("String " + getName(value)+ ";");
                    }
            }
        }).sheet().headRowNumber(0).doRead();

    }

    /**
     * print("D:\\temp\\fill.xlsx","字段名称","描述");
     * @param path
     * @param name
     * @param info
     */
    public static void print(
            String path,
            String name,
            String info
    ){
        FileUtilH.read(path,new Listener(){
            @Override
            public void invoke(Map<Integer, String> integerStringMap, AnalysisContext analysisContext) {
                super.invoke(integerStringMap, analysisContext);
                System.out.println("/**" + getByColName(info)+ "*/");
                System.out.println("String " + getByColName(name)+ ";");
            }
        });
    }

    /**
     * 下划线转大写
     * @param name a_b
     * @return aB
     */
    public static StringBuilder getName(String name) {
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<name.length();i++){
            if(name.charAt(i)=='_'){
                i++;
                //转大写
                if(Character.isLetter(name.charAt(i))){
                    sb.append((char)( name.charAt(i)-32));
                }else {
                    sb.append(name.charAt(i));
                }
            }
            else {
                sb.append(name.charAt(i));
            }
        }
        return sb;
    }

}
