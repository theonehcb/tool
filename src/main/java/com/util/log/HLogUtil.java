package com.util.log;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/12/13
 **/
@Slf4j
public class HLogUtil {
    public static void logBean(Object o){
        log.info(JSONUtil.parse(o).toStringPretty());
    }
}
