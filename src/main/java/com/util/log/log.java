package com.util.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import lombok.Data;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2023/10/12
 **/
public class log {
    public static void setLogLevel(){
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        List<ch.qos.logback.classic.Logger> loggerList = loggerContext.getLoggerList();
        for (int i = 0; i < loggerList.size(); i++) {
            loggerList.get(i).setLevel(Level.INFO);
        }
    }
}
