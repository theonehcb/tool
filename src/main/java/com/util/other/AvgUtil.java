package com.util.other;

import cn.hutool.core.util.NumberUtil;

/**
 * @Description: 用于求平均值
 * @Author: hechaobo
 * @Date: 2024/12/6
 **/
public class AvgUtil {
    double sum=0;
    int num=0;
    public void add(double v){
        num++;
        sum+=v;
    }
    public double getAvg(){
        if(num==0)return 0;
        return NumberUtil.div(sum,num);
    }
}
