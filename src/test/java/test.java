import com.baomidomybatisplus.samples.generator.system.service.PPPOEService;
import com.Application;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @Description: TODO
 * @Author: hechaobo
 * @Date: 2024/1/4
 **/
@SpringBootTest(classes = Application.class)
public class test {
    @Resource
    PPPOEService pppoeService;
    @Test
    public void test() throws InterruptedException {
        pppoeService.clear();
    }
}
